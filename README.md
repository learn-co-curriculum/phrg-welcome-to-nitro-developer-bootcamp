# Welcome

## Welcome to the Nitro Developer Bootcamp

Welcome to the Nitro Developer Bootcamp track. Over the next 6 months you will learn everything you need to know to get started as a Full Stack Web Developer at Power Home Remodeling Group. Before you get started, just a few thoughts about how to learn best using learn.co. It's going to be harder, and more rewarding than any other learning experience you've ever had, we promise. Let's learn together.

## Learning How to Learn

The most common misconception we get when people start this course is that we're teaching you Ruby, Javascript, HTML and CSS. Don't get me wrong, we are going to teach you those things, but that's actually not the most important set of things you're going to learn.

Over your career as a professional developer you're probably going to end up programming in languages that haven't been invented yet. The most important thing we're going to teach you is how to learn. Using the technologies above, we'll work with you to build the skills required to read and understand error messages and google for the answers so that when you have to learn a new library, framework or language, you'll know just how to go about doing so.

## The Learn IDE

When you first start learning to code, the amount of setup you have to do to get your computer ready to really *make* something can be frustrating. To counter this, we created the Learn IDE. The Learn IDE uses a popular programmers text editor (Atom) and also provides you with a terminal window for running commands, just as you would as a professional developer. However, instead of having to configure your local computer to allow you to program, it runs your commands on a remote computer so you can jump right into the programming - rather than spending hours configuring your computer.

## Local Development

But now that you have completed the Bootcamp Prep track and our together as a class, we can set up your local development environment. Please follow the instructions below and your instructor is here to help!

### Instructions WIP
